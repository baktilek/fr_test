import datetime

import pytz
import requests
from django.db.models import Q
from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from message.models import *
from message.serializers import *


class NewsLetterView(ModelViewSet):
    serializer_class = NewsLetterSerializer
    queryset = NewsLetter.objects.all()
    lookup_field = 'pk'

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return NewsLetterDetailSerializer
        elif self.action == 'list':
            return NewsLetterListSerializer
        else:
            return NewsLetterSerializer


class ClientView(ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    lookup_field = 'pk'


class ApplyView(ModelViewSet):
    def list(self, request, *args, **kwargs):
        now = datetime.datetime.now()
        letters = NewsLetter.objects.filter(start_time__lte=now, end_time__gte=now, is_done=False)
        fr_url = 'https://probe.fbrq.cloud/v1/send/'
        for letter in letters:
            clients = Client.objects.filter(Q(tag=letter.filter_tag) | Q(code=letter.filter_tag))
            for client in clients:
                now2 = datetime.datetime.now(datetime.timezone.utc)
                if now2 <= letter.end_time:
                    res = requests.post(fr_url+str(letter.id), json={
                        "id": letter.id,
                        "phone": client.phone,
                        "text": letter.text
                    }, headers={'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTI2OTE0OTYsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6ImJha3RpbGVrX3RhYWxhaWJla292In0.z-SuHPo8lP_hFPDfx7ZQ0Pt9fApS5ul01ET0w7h1SO4',
                                'Content-Type': 'application/json'
                                })
                    if res.status_code == 200:
                        message = Message(
                            status=True,
                            news_letter=letter,
                            client=client
                        )
                        message.save()
                    else:
                        message = Message(
                            status=False,
                            news_letter=letter,
                            client=client
                        )
                        message.save()
                else:
                    message = Message(
                        status=False,
                        news_letter=letter,
                        client=client
                    )
                    message.save()
            letter.is_done = True
            letter.save()
        return Response({'message': 'Tasks is DONE!'}, status=status.HTTP_200_OK)


