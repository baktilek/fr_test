from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

from message.views import *

schema_view = get_swagger_view(title='Pastebin API')
urlpatterns = [
    path('news_letter', NewsLetterView.as_view({'post': 'create', 'get': 'list'})),
    path('news_letter/<int:pk>',
         NewsLetterView.as_view({'put': 'update', 'get': 'retrieve', 'delete': 'destroy', 'patch': 'partial_update'})),
    path('client', ClientView.as_view({'post': 'create'})),
    path('client/<int:pk>', ClientView.as_view({'put': 'update', 'delete': 'destroy', 'patch': 'partial_update'})),
    path('apply/tasks', ApplyView.as_view({'get': 'list'}))
]
