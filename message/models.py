from django.db import models


class NewsLetter(models.Model):
    start_time = models.DateTimeField()
    text = models.TextField()
    filter_tag = models.CharField(max_length=30)
    end_time = models.DateTimeField()
    is_done = models.BooleanField(default=False)


class Client(models.Model):
    phone = models.PositiveIntegerField()
    code = models.CharField(max_length=20)
    tag = models.CharField(max_length=20)
    time_zone = models.IntegerField()

    def __str__(self):
        return str(self.phone)


class Message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=True)
    news_letter = models.ForeignKey(NewsLetter, models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, models.CASCADE, related_name='messages')


