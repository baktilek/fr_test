from rest_framework import serializers

from message.models import *


class NewsLetterSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsLetter
        fields = ['start_time', 'text', 'filter_tag', 'end_time']


class NewsLetterDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsLetter
        fields = ['id', 'start_time', 'text', 'filter_tag', 'end_time']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        phone_success = []
        for i in instance.messages.filter(status=True):
            phone_success.append({'phone': i.client.phone})
        phone_fail = []
        for i in instance.messages.filter(status=False):
            phone_fail.append({'phone': i.client.phone})
        representation['message'] = {
            'all': instance.messages.all().count(),
            'success': phone_success,
            'fail': phone_fail,
        }
        return representation


class NewsLetterListSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsLetter
        fields = ['id', 'start_time', 'text', 'filter_tag', 'end_time']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['message'] = {
            'all': instance.messages.all().count(),
            'success': instance.messages.filter(status=True).count(),
            'fail': instance.messages.filter(status=False).count(),
        }
        return representation


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['phone', 'code', 'tag', 'time_zone']
