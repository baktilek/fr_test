from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view
schema_view = get_swagger_view(title='ФР API')
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('message.urls')),
    url(r'^$', schema_view)
]
